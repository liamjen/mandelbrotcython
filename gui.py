import sys
import re
from PySide2.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QLineEdit, QLabel, QScrollArea, QShortcut
from PySide2.QtGui import QPixmap, QImage, QKeySequence
from PySide2.QtCore import Qt

from PIL.ImageQt import ImageQt
from brotc import Mandelbrot

from dataclasses import dataclass

# Defaults
WIDTH  = 800
HEIGHT = 800

class MandelbrotGUI(QWidget):
    def __init__(self):
        self.app = QApplication(sys.argv)
        super().__init__()
        self.initialize()

    def initialize(self):
        self.setWindowTitle("Mandelbrot")
        self.resize(WIDTH, HEIGHT)
        main_layout = QVBoxLayout()
        main_layout.addStretch(1)
        self.controls = Controls(self)
        self.mandelbrot_scroll_area = MandelbrotScrollArea(self)
        main_layout.addWidget(self.mandelbrot_scroll_area, stretch=100)
        main_layout.addWidget(self.controls)
        self.setLayout(main_layout)
        # Reset mandelbrot limits shortcut -> "R"
        QShortcut(QKeySequence("r"), self, activated=self.controls.reset_coordinate_limits)
        # Normalize mandelbrot limits shortcut -> "N"
        QShortcut(QKeySequence("n"), self, activated=self.controls.normalize_coordinate_limits)
        # Pan image shortcuts
        QShortcut(QKeySequence("h"), self, activated=self.controls.pan_image_left)
        QShortcut(QKeySequence("j"), self, activated=self.controls.pan_image_down)
        QShortcut(QKeySequence("k"), self, activated=self.controls.pan_image_up)
        QShortcut(QKeySequence("l"), self, activated=self.controls.pan_image_right)

        # Force update image on init
        self.controls.update_image()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        sys.exit(self.app.exec_())

    def set_image(self, image):
        self.mandelbrot_scroll_area.mandelbrot_view.set_image(image)

    def get_current_control_state(self):
        return self.controls.current_control_state

    def set_coordinate_limits(self, x_min, x_max, y_min, y_max):
        self.controls.set_coordinate_limits(x_min, x_max, y_min, y_max)

    def start():
        self.show()

class MandelbrotScrollArea(QScrollArea):
    """
    Holds the MandelbrotView image to allow scrolling and panning
    """
    def __init__(self, root_widget):
        super().__init__()
        self.setWidgetResizable(True)
        self.root_widget = root_widget
        self.initialize()

    def initialize(self):
        self.mandelbrot_view = MandelbrotView(self.root_widget)
        self.setWidget(self.mandelbrot_view)

class MandelbrotView(QLabel):
    """
    QLabel holding the Pixmap of the Mandelbrot image.
    This class also handles logic for clicking on portions of the image and zooming.
    """
    def __init__(self, root_widget):
        super().__init__()
        self.root_widget = root_widget
        self.box_selecting = False # Controls box select on click
        self.last_click_coords = (0.0,0.0)
        self.initialize()

    def initialize(self):
        self.mousePressEvent = self.on_click_event
        self.setAlignment(Qt.AlignCenter)

    def set_image(self, image):
        self.pixmap = QPixmap.fromImage(image)
        self.setPixmap(self.pixmap)

    def on_click_event(self, mouseEvent):
        image_click_xpos = mouseEvent.x() - ((self.width() - self.pixmap.width()) / 2)
        image_click_ypos = mouseEvent.y() - ((self.height() - self.pixmap.height()) / 2)

        if image_click_xpos > self.pixmap.width() or image_click_xpos < 0:
            print("outtabounds")
        elif image_click_ypos > self.pixmap.height() or image_click_ypos < 0:
            print("outtabounds")
        else:
            c = self.root_widget.get_current_control_state()
            x_range = (abs(c.x_min) + c.x_max) if c.x_min < 0 else (c.x_max - c.x_min)
            y_range = (abs(c.y_min) + c.y_max) if c.y_min < 0 else (c.y_max - c.y_min)
            x_coord = (image_click_xpos * x_range / self.pixmap.width()) + c.x_min
            y_coord = -((image_click_ypos * y_range / self.pixmap.height()) - c.y_max)

            if self.box_selecting:
                x_min, x_max, y_min, y_max = MandelbrotView.find_box_bounds(x_coord, self.last_click_coords[0], y_coord, self.last_click_coords[1])
                self.root_widget.set_coordinate_limits(x_min, x_max, y_min, y_max)
                self.box_selecting = False
            else:
                self.box_selecting = True
                self.last_click_coords = (x_coord, y_coord)
                            

    def find_box_bounds(x1, x2, y1, y2): #returns correct x_min, x_max, etc...
        return (min(x1, x2), max(x1, x2), min(y1, y2), max(y1, y2))



@dataclass
class ControlState():
    """
    Class to hold the last used parameters for generating mandelbrot set 
    """
    x_min: float
    x_max: float
    y_min: float
    y_max: float
    x_size: int
    y_size: int
    num_iterations: int

class Controls(QWidget):
    def __init__(self, root_widget):
        super().__init__()
        self.current_control_state = ControlState(0.0, 0.0, 0.0, 0.0, 0, 0, 0)
        self.root_widget = root_widget
        self.initialize()

    def initialize(self):
        self.main_layout = QVBoxLayout()
        self.init_text_controls()
        self.setLayout(self.main_layout)

    def init_text_controls(self):
        self.init_text_dimensional_controls()
        self.init_text_extra_controls()

    class LineEditLabelLayout(QVBoxLayout):
        """
        Helper class to add a label on top of an array of line edit objects.
        Label is centered vertically above line edits(in horizontal layout)
        """
        def __init__(self, line_edits, label_text):
            super().__init__()
            self.label = QLabel(label_text, alignment=Qt.AlignCenter)
            self.addWidget(self.label)
            self.line_edit_layout = QHBoxLayout()
            for line_edit in line_edits:
                self.line_edit_layout.addWidget(line_edit)

            self.addLayout(self.line_edit_layout)

        def set_label_text(self, label_text):
            self.label.setText(str(label_text))

        def replace_in_label_text(self, pattern, repl):
            self.label.setText(re.sub(pattern, str(repl), self.label.text()))

    def init_text_dimensional_controls(self):
        layout = QHBoxLayout()
        self.x_min_control = QLineEdit("-2.0")
        self.x_max_control = QLineEdit("1.0")
        self.y_min_control = QLineEdit("-1.0")
        self.y_max_control = QLineEdit("1.0")

        self.x_min_control.returnPressed.connect(self.update_image)
        self.x_max_control.returnPressed.connect(self.update_image)
        self.y_min_control.returnPressed.connect(self.update_image)
        self.y_max_control.returnPressed.connect(self.update_image)

        layout.addLayout(Controls.LineEditLabelLayout([self.x_min_control,self.x_max_control], "x coordinate limits"))
        layout.addLayout(Controls.LineEditLabelLayout([self.y_min_control,self.y_max_control], "y coordinate limits"))

        self.main_layout.addLayout(layout)

    def init_text_extra_controls(self):
        layout = QHBoxLayout()
        self.x_size_control = QLineEdit("900")
        self.y_size_control = QLineEdit("600")
        self.num_iterations_control = QLineEdit("150")

        self.x_size_control.returnPressed.connect(self.update_image)
        self.y_size_control.returnPressed.connect(self.update_image)
        self.num_iterations_control.returnPressed.connect(self.update_image)

        # Make instance variable to change label text when controls change
        self.iterations_control_layout = Controls.LineEditLabelLayout([self.num_iterations_control], "max function iterations: [max function calls: 0.0E+0]")
        layout.addLayout(Controls.LineEditLabelLayout([self.x_size_control,self.y_size_control], "image resolution"))
        layout.addLayout(self.iterations_control_layout)

        self.main_layout.addLayout(layout)

    def set_coordinate_limits(self, x_min, x_max, y_min, y_max):
        """
        Sets the x and y coordinate limits of the generated Mandelbrot image and generates
        the image.
        """
        current_controls = self.get_control_state()
        current_controls.x_min = x_min
        current_controls.x_max = x_max
        current_controls.y_min = y_min
        current_controls.y_max = y_max
        self.set_control_state(current_controls)
        self.update_image()

    def reset_coordinate_limits(self):
        """
        Resets the Mandelbrot image to the original coordinate limits.
        x: [-2.0, 1.0]
        y: [-1.0, 1.0]
        """
        self.set_coordinate_limits(-2.0, 1.0, -1.0, 1.0)
        self.set_control_state(self.get_control_state())

    def normalize_coordinate_limits(self):
        """
        Sets the coordinate limits to the original aspect ratio, but retains
        the zoom level.

        Correct aspect ratio = 3:2
        """
        ctrl = self.get_control_state()
        x_range = ctrl.x_max - ctrl.x_min
        set_y_range = x_range * 0.67 # 3:2 ratio
        ctrl.y_max = ctrl.y_min + set_y_range
        self.set_coordinate_limits(ctrl.x_min, ctrl.x_max, ctrl.y_min, ctrl.y_max)
        self.set_control_state(ctrl)

    def pan_image(self, left=0, right=0, down=0, up=0):
        """
        Pans the mandelbrot image a percentage of the image size in any direction.
        0 = 0%
        5 = 5%
        """
        ctrl = self.get_control_state()
        x_range = ctrl.x_max - ctrl.x_min
        y_range = ctrl.y_max - ctrl.y_min
        pan_left  = x_range * (.01 * left)
        pan_right = x_range * (.01 * right)
        pan_down  = y_range * (.01 * down)
        pan_up    = y_range * (.01 * up)
        ctrl.x_min = ctrl.x_min - pan_left + pan_right
        ctrl.x_max = ctrl.x_max - pan_left + pan_right
        ctrl.y_min = ctrl.y_min - pan_down + pan_up
        ctrl.y_max = ctrl.y_max - pan_down + pan_up
        self.set_coordinate_limits(ctrl.x_min, ctrl.x_max, ctrl.y_min, ctrl.y_max)
        self.set_control_state(ctrl)

    def pan_image_left(self):
        self.pan_image(left=10)

    def pan_image_right(self):
        self.pan_image(right=10)

    def pan_image_down(self):
        self.pan_image(down=10)

    def pan_image_up(self):
        self.pan_image(up=10)

    def get_control_state(self): # Returns control state based on text boxes
        return ControlState(float(self.x_min_control.text()), float(self.x_max_control.text()), float(self.y_min_control.text()), float(self.y_max_control.text()), int(self.x_size_control.text()), int(self.y_size_control.text()), int(self.num_iterations_control.text()))

    def set_control_state(self, control_state): # Sets text box contents to control_state
        self.x_min_control.setText(str(control_state.x_min))
        self.x_max_control.setText(str(control_state.x_max))
        self.y_min_control.setText(str(control_state.y_min))
        self.y_max_control.setText(str(control_state.y_max))
        self.x_size_control.setText(str(control_state.x_size))
        self.y_size_control.setText(str(control_state.y_size))
        self.num_iterations_control.setText(str(control_state.num_iterations))

    def update_image(self):
        new_control_state = self.get_control_state()

        if new_control_state != self.current_control_state: # Do not update if same params
            self.current_control_state = new_control_state
            max_function_calls = new_control_state.x_size * new_control_state.y_size * new_control_state.num_iterations
            self.iterations_control_layout.replace_in_label_text(r"\d+\.*\d*E\+\d*", format(max_function_calls, "1.2E"))
            self.q_img = self.get_q_image(new_control_state) # Keep reference to qimg object or else gui crashes on resize
            self.root_widget.set_image(self.q_img)

    def get_q_image(self, control_state):
        """
        Calculates the Mandelbrot image based on control state.

        Reurns an image the Qt UI can display.
        """
        img = Mandelbrot.find_set_to_image(
                control_state.x_size, 
                control_state.y_size, 
                n_loops=control_state.num_iterations, 
                X_MIN=control_state.x_min, 
                Y_MIN=control_state.y_min, 
                X_MAX=control_state.x_max, 
                Y_MAX=control_state.y_max)
        return ImageQt(img)


