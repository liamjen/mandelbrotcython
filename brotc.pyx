# distutils: language = c++
import numpy as np
cimport numpy as np
cimport cython
from PIL import Image

cdef struct MandelIter:
    # Holds mandelbrot function values (real and complex broken into x and y)
    double x, y

cdef class Mandelbrot():
    @staticmethod
    cdef int brot(double real, double cmplx, int n_loops=100, double tolerance=5):
        """
        Calculates if a given point (E.g. real, complex value) is within the Mandelbrot set.
        
        Returns how many times the function had to repeat to become larger than 'tolerance'
        """
        cdef:
            int i # Loop iteration variable
            # n_iter holds the current Mandelbrot x and y values returned by calling the function on itself
            MandelIter n_iter = MandelIter(0.0, 0.0)
        
        for i in range(n_loops):
            n_iter = MandelIter(((n_iter.x**2.0) - (n_iter.y**2.0)) + real, (2.0*n_iter.x*n_iter.y) + cmplx)
            
            if abs(n_iter.x) > tolerance or abs(n_iter.y) > tolerance:
                return i

        return n_loops

    @cython.boundscheck(False)      # turn off bounds-checking for entire function
    @cython.wraparound(False)       # turn off negative index wrapping for entire function
    @cython.initializedcheck(False) # dont check memory views are initialized
    @cython.cdivision(True)         # divisions cannot throw python runtime errors
    @staticmethod
    def find_set_to_image(int x_dims, int y_dims, int n_loops=100, 
            double X_MIN=-2.0, double Y_MIN=-1.0, double X_MAX=1.0, double Y_MAX=1):
        """
        Calculates and returns a Mandelbrot image of given x and y dimensions 
        with x and y coordinate limits.
        
        n_loops: number of times to call Mandelbrot function on a given
          point before determining if the point does/not diverge.

        Image is Pillow Python Image Library image.
        """
    
        if X_MIN >= X_MAX:
            raise ValueError(f"X_MIN {X_MIN} greater than X_MAX {X_MAX}")
        elif Y_MIN >= Y_MAX:
            raise ValueError(f"Y_MIN {Y_MIN} greater than Y_MAX {Y_MAX}")

        cdef:
            double x_step = (abs(X_MIN) + X_MAX) / x_dims if X_MIN < 0 else (X_MAX - X_MIN) / x_dims
            double y_step = (abs(Y_MIN) + Y_MAX) / y_dims if Y_MIN < 0 else (Y_MAX - Y_MIN) / x_dims
            double real_part = X_MIN    # Leftmost
            double cmplx_part = Y_MAX   # Topmost
            int div_speed 
            int min_diverge_loops = n_loops
            int max_diverge_loops = 0
            int diverge_range = 0
            int max_color = 255, num_colors = 3
            unsigned int color
            np.ndarray[unsigned char, ndim=3] color_data_view = np.zeros([y_dims, x_dims, 3], dtype=np.uint8)
            np.ndarray[unsigned char, ndim=1] current_color = np.zeros([3], dtype=np.uint8)
            # Use numpy array views for accessing numpy arrays without python interpreter
            unsigned char[:] curr_col_view = current_color
            unsigned char[:, :, :] col_data_view = color_data_view

        
        # Calculate Mandelbrot divergence speeds
        for x in range(x_dims):
            for y in range(y_dims):
                div_speed = Mandelbrot.brot(real_part, cmplx_part, n_loops=n_loops)
                if div_speed > max_diverge_loops:
                    max_diverge_loops = div_speed
                elif div_speed < min_diverge_loops:
                    min_diverge_loops = div_speed

                curr_col_view[0] = div_speed
                col_data_view[y][x] = curr_col_view # temporarily store div speed at index 0 to determine color later

                cmplx_part -= y_step
            real_part += x_step
            cmplx_part = Y_MAX

        # Create color array for PIL image creation
        diverge_range = max_diverge_loops - min_diverge_loops # Range to map colors to
        for x in range(x_dims):
            for y in range(y_dims):
                color = (max_color*num_colors) * (col_data_view[y][x][0]) / diverge_range
                if color < 255:
                    Mandelbrot.set_color_view_colors(curr_col_view, 0, 0+color, 0+color)
                    #col_data_view[y][x] = [0, 0 + color, 0 + color]
                    col_data_view[y][x] = curr_col_view
                elif color < (2*255):
                    Mandelbrot.set_color_view_colors(curr_col_view, 0, 255-color, 0+color)
                    col_data_view[y][x] = curr_col_view
                else:
                    Mandelbrot.set_color_view_colors(curr_col_view, 255-color, 0, 0)
                    col_data_view[y][x] = curr_col_view

        return Image.fromarray(color_data_view)       # Create a PIL image

    @cython.boundscheck(False)      # turn off bounds-checking for entire function
    @cython.wraparound(False)       # turn off negative index wrapping for entire function
    @cython.initializedcheck(False) # dont check memory views are initialized
    @staticmethod
    cdef void set_color_view_colors(unsigned char[:] view, unsigned char red, unsigned char green, unsigned char blue):
        # Sets the 3 array elements to RGB values (to be used by PIL image function)
        view[0] = red
        view[1] = green
        view[2] = blue
        

    @staticmethod
    def draw_image(int x_dims, int y_dims, dict full_set, color=False):
        color_data = np.zeros((y_dims,x_dims,3), dtype=np.uint8)
        # data[512,512] = [254,0,0] Makes the middle pixel red
        for coords, divergence in full_set.items():
            if color:
                div_speed = 1/divergence
                color = (255*3)*div_speed
                if color < 10:
                    color_data[coords[1], coords[0]] = [255, 255, 255]
                elif color < 255:
                    color_data[coords[1], coords[0]] = [255, 0, 255 - color]
                elif color < (2*255):
                    color_data[coords[1], coords[0]] = [255, 255 - color, 0]
                else:
                    color_data[coords[1], coords[0]] = [255 - color, 0, 0]
            else:
                if full_set[(coords[0], coords[1])] >= 100:
                    color_data[coords[1], coords[0]] = [255,255,255]
            
        return color_data

    @staticmethod
    def show_image(color_data):
        img = Image.fromarray( color_data )       # Create a PIL image
        img.save("brot.png", "PNG")                      # View in default viewer

