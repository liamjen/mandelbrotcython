# About
## MandelbrotCython

Mandelbrot set viewer written in python (PySide2 gui) and leveraging cython for back end computing.

Image is displayed with x values being the "real" part of the Mandelbrot function and y values being the "complex" part (which is convention). Varying colors on the image denote the speed at which a given point diverges towards infinity, or how many times the Mandelbrot function had to repeat for the value to exceed a tolerance value.

![MandelbrotCython](https://i.imgur.com/f2jlfWf.png)

See the [Mandelbrot Wikipedia page](https://en.wikipedia.org/wiki/Mandelbrot_set) for more information

# Setup

## Optional *(Recommended)*: Virtual Environment
```
python -m venv env
source env/bin/activate
```
*Note: This allows the dependencies to install within the mandelbrotcython directory*
## Building and Running
```
python setup.py install
python main.py
```

# Controls
|Control|Output|
|---|---|
|**Click**|Click on two points on the image to zoom to a bounding box of the two click locations|
|**Enter**| Press enter while highlighting any line of editable text to update the image with the set parameters|
|**R**| Reset the coordinate limits of the Mandelbrot set. (E.g. `x: [-2.0, 1.0] y: [-1.0, 1.0]`)|
|**N**| Normalize the coordinate limits of the Mandelbrot set. This forces a 3:2 aspect ratio, which is the original aspect viewing ratio|
|**H**| Pan Mandelbrot image 10% left|
|**J**| Pan Mandelbrot image 10% down|
|**K**| Pan Mandelbrot image 10% up|
|**L**| Pan Mandelbrot image 10% right|
|**x coordinate limits**|Sets minimum and maximum range of x values (real) displayed on the image|
|**y coordinate limits**|Sets minimum and maximum range of y values (imaginary) displayed on the image|
|**image resolution**| Sets size of output image displayed (width x height)|
|**max function iterations**|Sets number of times to check any given point to determine of that point is a part of the Mandelbrot set. Increasing this value increases runtime complexity linearly|

