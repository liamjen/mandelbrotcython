from gui import MandelbrotGUI
from brotc import Mandelbrot

from PIL import Image

# Create and show GUI, which handles all user input and processing
with MandelbrotGUI() as m_gui:
    m_gui.show()
