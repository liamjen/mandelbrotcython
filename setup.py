from setuptools import setup, Extension, dist

# Install required modules for building program
# Cython - Build utils
# numpy - numpy include path
dist.Distribution().fetch_build_eggs(['Cython>=0.15.1', 'numpy>=1.10'])
import numpy
from Cython.Build import cythonize

# Mandelbrot Cython library 
extensions = [
    Extension("brotc", ["brotc.pyx"],
              include_dirs=[numpy.get_include()])
]

setup(
 name="MandelbrotCython",
 version="1.0",
 author="liam",
 setup_requires=["setuptools>=18.0", "cython", "numpy"],
 ext_modules=cythonize(extensions, annotate=True),
 install_requires=["PySide2", "Pillow", "numpy", "cython"],
 zip_safe=False
)
